# i-made-this
---

![](img/i-made-this.png)

`i-made-this` is the best way to quickly deploy high-quality projects on GitHub.

Why struggle through building something impressive when plenty of developers have already done it?

**Just copy someone else's code and take credit for it!**

Most devs have to go through the entire software development process. This is a long journey. It involves:

1. Coming up with a good idea.
2. Planning your implementation.
3. Developing the program.
4. Testing your code.
5. Refining it to fix any bugs you discover in testing.
6. Releasing the software, and hoping to be recognized for your hard work and dedication.

With `i-made-this`, you can skip directly to step 6!

**What does it do?**

This script clones an existing github repository, renames it and publishes it to your github profile after removing the git history and license.

### Usage Instructions
---

0. Create a new repository on GitHub for your hard work to be hosted in.
1. Navigate to the local directory you'd like to "build your project" in.
2. `$ ./i-made-this` and follow the prompts.

### Why?
---

I wrote this script to satirize how commonplace it's become for developers of all skill levels to steal code from others without attributing it to the real author.

### License(d)
---

Ironically.

### Acknowledgements
---

+ Comic by [neodroidcomics](https://nedroidcomics.tumblr.com/post/41879001445/the-internet)
